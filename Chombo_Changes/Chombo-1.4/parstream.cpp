/* _______              __
  / ___/ /  ___  __ _  / /  ___
 / /__/ _ \/ _ \/  ' \/ _ \/ _ \
 \___/_//_/\___/_/_/_/_.__/\___/ 
*/
//
// This software is copyright (C) by the Lawrence Berkeley
// National Laboratory.  Permission is granted to reproduce
// this software for non-commercial purposes provided that
// this notice is left intact.
// 
// It is acknowledged that the U.S. Government has rights to
// this software under Contract DE-AC03-765F00098 between
// the U.S.  Department of Energy and the University of
// California.
//
// This software is provided as a professional and academic
// contribution for joint exchange. Thus it is experimental,
// is provided ``as is'', with no warranties of any kind
// whatsoever, no support, no promise of updates, or printed
// documentation. By using this software, you acknowledge
// that the Lawrence Berkeley National Laboratory and
// Regents of the University of California shall have no
// liability with respect to the infringement of other
// copyrights by any part of this software.
//

#include <cstdio>
#include <string>
#include <fstream>
#include "parstream.H"
using std::ostream;
using std::ofstream;

#include "SPMD.H"


ostream& pout ()
{  
#ifdef CH_MPI
  int flag_i,flag_f;
  MPI_Initialized( &flag_i );
  MPI_Finalized( &flag_f );
  if ((flag_i == 1) && (flag_f == 0))
  {
    static char buffer[137];
    static int temp = sprintf (buffer, "%d", procID ());
    static std::string par_name = std::string ("pout.") + buffer;
    static ofstream s_pout (par_name.c_str());

    return (s_pout);
  }
  else
  {
    return std::cout;  // MPI hasn't been started yet....
  }
#else
  return (std::cout);
#endif
}
