Changes in "\lib\src\AMRTimeDependent\AMR.h"
   1. setupForRestart is redeclared as virtual
   2. writePlotFile is redeclared as virtual
   3. run is redeclared as virtual
   4. writeCheckpointFile is redeclared as virtual

Changes in "\lib\src\BoxTools\parstream.cpp"
   1. in pout() an additional check is added if MPI is finilized.